package com.sox.gateway.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class AuthenticationResponse {

    private String token;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private Set<Integer> roles;
}
