package com.sox.gateway.payload;

import lombok.Data;

@Data
public class SOXResponse<T> {

    private T data;
    private String errorMsg;

}
