package com.sox.gateway.service;

import com.sox.gateway.entity.UserDetails;
import com.sox.gateway.entity.UserInfo;
import com.sox.gateway.entity.UserRoleDetails;
import com.sox.gateway.repository.UserDetailsRepository;
import com.sox.gateway.repository.UserInfoRepository;
import com.sox.gateway.repository.UserRoleDetailsRepository;
import com.sox.gateway.utility.RoleUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AuthService {

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    public User getUserByUsername(String username){
        Set<GrantedAuthority> authorities = new HashSet<>();
        List<UserRoleDetails> roleDetailsList = userRoleDetailsRepository.findByUserIdAndActive(username, true);
        String userName;
        if(roleDetailsList.isEmpty()){
            Optional<UserDetails> userDetails = userDetailsRepository.findById(username);
            if(userDetails.isPresent()){
                userName = userDetails.get().getUserId();
            }else{
                throw new UsernameNotFoundException(username);
            }
        }else{
            roleDetailsList
                    .forEach(role -> authorities.add(new SimpleGrantedAuthority(RoleUtility.getRoleName(role.getRoleId()))));
            userName = roleDetailsList.get(0).getUserId();
        }

        return new User(userName,"password", authorities);
    }

    public List<UserRoleDetails> getRoleDetails(String userId){
        return userRoleDetailsRepository.findByUserIdAndActive(userId, true);
    }

    public UserInfo getUserInfo(String userId){
        return userInfoRepository.findById(userId).orElseGet(UserInfo::new);
    }
}
