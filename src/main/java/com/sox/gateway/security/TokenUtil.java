package com.sox.gateway.security;

import com.sox.gateway.entity.UserInfo;
import com.sox.gateway.repository.UserInfoRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Service
public class TokenUtil {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Value("{jwt.secret}")
    private String SECRET_KEY;

    public <T> T extractClaims(String token, Function<Claims, T> claimsResolver){
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);

    }

    public String extractUsername(String token){
        return extractClaims(token, Claims::getSubject);
    }

    public Date extractExpirationTime(String token){
        return extractClaims(token , Claims::getExpiration);
    }

    private Claims extractAllClaims(String token){
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token){
        return extractExpirationTime(token).before(new Date());
    }

    public String generateToken(UserDetails userData){
        Map<String, Object> claims = new HashMap<>();
        Optional<UserInfo> userInfo = userInfoRepository.findById(userData.getUsername().toLowerCase());
        userInfo.ifPresent(info -> claims.put("user", info));
        return createJWTToken(claims, userData.getUsername().toLowerCase());
    }

    private String createJWTToken(Map<String, Object> claims, String subject){
        return Jwts
                .builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 10*60*60*60*10))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userData){
        final String userName = extractUsername(token);
        return (userName.equals(userData.getUsername()) && !isTokenExpired(token));
    }

}
