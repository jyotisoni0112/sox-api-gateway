package com.sox.gateway.repository;

import com.sox.gateway.entity.UserRoleDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleDetailsRepository extends JpaRepository<UserRoleDetails, Integer> {

    List<UserRoleDetails> findByUserId(String userId);
    List<UserRoleDetails> findByUserIdAndActive(String userId, boolean active);

}
