package com.sox.gateway.utility;

public class RoleUtility {

    public static final int ROLE_ADMIN = 9;
    public static final int ROLE_DASHBOARD_ADMIN = 10;
    public static final int ROLE_BUSINESS_POC_1 = 1;
    public static final int ROLE_BUSINESS_B_POC_1 = 2;
    public static final int ROLE_BUSINESS_POC_2 = 3;
    public static final int ROLE_BUSINESS_B_POC_2 = 4;
    public static final int ROLE_BUSINESS_POC_3 = 5;
    public static final int ROLE_BUSINESS_B_POC_3 = 6;
    public static final int ROLE_READ_ONLY = 7;
    public static final int ROLE_IT_OWNER = 12;
    public static final int ROLE_BUSINESS_OWNER = 13;
    public static final int ROLE_AUDITOR_DELOITTE = 14;
    public static final int ROLE_MJE_ADMIN = 15;
    public static final int ROLE_MJE_REVIEWER = 16;
    public static final int ROLE_PAC_ADMIN = 17;
    public static final int ROLE__PAC_REVIEWER = 18;
    public static final int ROLE_USER_STATUS_ADMIN = 19;
    public static final int ROLE_USER_STATUS_USER = 20;
    public static final int ROLE_AUDITOR_EY = 21;
    public static final int ROLE_GCA_SOX_CHAMP = 22;
    public static final int ROLE_GCA_USERS = 23;

    public static final String ROLE_NAME_ADMIN = "ADMIN";
    public static final String ROLE_NAME_DASHBOARD_ADMIN = "DASHBOARD_ADMIN";
    public static final String ROLE_NAME_BUSINESS_POC_1 = "BUSINESS_POC_1";
    public static final String ROLE_NAME_BUSINESS_B_POC_1 = "BUSINESS_POC_B_1";
    public static final String ROLE_NAME_BUSINESS_POC_2 = "BUSINESS_POC_2";
    public static final String ROLE_NAME_BUSINESS_B_POC_2 = "BUSINESS_POC_B_2";
    public static final String ROLE_NAME_BUSINESS_POC_3 = "BUSINESS_POC_3";
    public static final String ROLE_NAME_BUSINESS_B_POC_3 = "BUSINESS_POC_B_3";
    public static final String ROLE_NAME_READ_ONLY = "READ_ONLY";
    public static final String ROLE_NAME_IT_OWNER = "IT_OWNER";
    public static final String ROLE_NAME_BUSINESS_OWNER = "BUSINESS_OWNER";
    public static final String ROLE_NAME_AUDITOR_DELOITTE = "AUDITOR_DELOITTE";
    public static final String ROLE_NAME_MJE_ADMIN = "MJE_ADMIN";
    public static final String ROLE_NAME_MJE_REVIEWER = "MJE_REVIEWER";
    public static final String ROLE_NAME_PAC_ADMIN = "PAC_ADMIN";
    public static final String ROLE_NAME__PAC_REVIEWER = "PAC_REVIEWER";
    public static final String ROLE_NAME_USER_STATUS_ADMIN = "USER_STATUS_ADMIN";
    public static final String ROLE_NAME_USER_STATUS_USER = "USER_STATUS";
    public static final String ROLE_NAME_AUDITOR_EY = "AUDITOR_EY";
    public static final String ROLE_NAME_GCA_SOX_CHAMP = "GCA_CHAMP";
    public static final String ROLE_NAME_GCA_USERS = "GCA_USER";

    public static String getRoleName(int roleId){
        switch (roleId){
            case ROLE_BUSINESS_POC_1: return ROLE_NAME_BUSINESS_POC_1;
            case ROLE_BUSINESS_B_POC_1: return ROLE_NAME_BUSINESS_B_POC_1;
            case ROLE_BUSINESS_POC_2: return ROLE_NAME_BUSINESS_POC_2;
            case ROLE_BUSINESS_B_POC_2: return ROLE_NAME_BUSINESS_B_POC_2;
            case ROLE_BUSINESS_POC_3: return ROLE_NAME_BUSINESS_POC_3;
            case ROLE_BUSINESS_B_POC_3: return ROLE_NAME_BUSINESS_B_POC_3;
            case ROLE_READ_ONLY: return ROLE_NAME_READ_ONLY;
            case ROLE_ADMIN: return ROLE_NAME_ADMIN;
            case ROLE_DASHBOARD_ADMIN: return ROLE_NAME_DASHBOARD_ADMIN;
            case ROLE_IT_OWNER: return ROLE_NAME_IT_OWNER;
            case ROLE_BUSINESS_OWNER: return ROLE_NAME_BUSINESS_OWNER;
            case ROLE_AUDITOR_DELOITTE: return ROLE_NAME_AUDITOR_DELOITTE;
            case ROLE_MJE_ADMIN: return ROLE_NAME_MJE_ADMIN;
            case ROLE_MJE_REVIEWER: return ROLE_NAME_MJE_REVIEWER;
            case ROLE_PAC_ADMIN: return ROLE_NAME_PAC_ADMIN;
            case ROLE__PAC_REVIEWER: return ROLE_NAME__PAC_REVIEWER;
            case ROLE_USER_STATUS_ADMIN: return ROLE_NAME_USER_STATUS_ADMIN;
            case ROLE_USER_STATUS_USER: return ROLE_NAME_USER_STATUS_USER;
            case ROLE_AUDITOR_EY: return ROLE_NAME_AUDITOR_EY;
            case ROLE_GCA_SOX_CHAMP: return ROLE_NAME_GCA_SOX_CHAMP;
            case ROLE_GCA_USERS: return ROLE_NAME_GCA_USERS;
            default:return null;
        }
    }

    private RoleUtility(){}
}
