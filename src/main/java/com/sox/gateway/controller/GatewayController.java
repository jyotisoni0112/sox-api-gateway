package com.sox.gateway.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {

    @GetMapping("/gateway")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String gateway(){
        return "SOX Gateway ";
    }

}
