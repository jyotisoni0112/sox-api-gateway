package com.sox.gateway.controller;

import com.sox.gateway.entity.UserInfo;
import com.sox.gateway.entity.UserRoleDetails;
import com.sox.gateway.payload.AuthenticationResponse;
import com.sox.gateway.payload.AuthenticationRequest;
import com.sox.gateway.security.CustomUserDetailsService;
import com.sox.gateway.security.TokenUtil;
import com.sox.gateway.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest authenticationRequest){
        ResponseEntity<AuthenticationResponse> responseEntity;
        try{
            if(authenticationRequest.getUsername().isEmpty() || authenticationRequest.getPassword().isEmpty()){
                responseEntity = ResponseEntity.badRequest().build();
            }else{
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(), authenticationRequest.getPassword()
                ));
                final UserDetails userDetails = customUserDetailsService.loadUserByUsername(authenticationRequest.getUsername().toLowerCase());
                AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                authenticationResponse.setToken(tokenUtil.generateToken(userDetails));
                List<UserRoleDetails> roleDetailsList = authService.getRoleDetails(authenticationRequest.getUsername().toLowerCase());
                if(roleDetailsList.isEmpty()){
                    UserInfo userInfo = authService.getUserInfo(authenticationRequest.getUsername().toLowerCase());
                    authenticationResponse.setUsername(userInfo.getUserId());
                    authenticationResponse.setFirstName(userInfo.getFirstName());
                    authenticationResponse.setLastName(userInfo.getLastName());
                    authenticationResponse.setEmail(userInfo.getEmail());
                }else{
                    Set<Integer> roleList = new HashSet<>();
                    roleDetailsList.forEach(role -> roleList.add(role.getRoleId()));
                    authenticationResponse.setRoles(roleList);
                    authenticationResponse.setUsername(roleDetailsList.get(0).getUserId());
                    authenticationResponse.setFirstName(roleDetailsList.get(0).getUserInfo().getFirstName());
                    authenticationResponse.setLastName(roleDetailsList.get(0).getUserInfo().getLastName());
                    authenticationResponse.setEmail(roleDetailsList.get(0).getUserInfo().getEmail());
                }
                responseEntity = ResponseEntity.ok().body(authenticationResponse);
            }
        }catch (BadCredentialsException exception){
            responseEntity = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }catch (Exception exception){
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return responseEntity;
    }
}
